#!/usr/bin/python

#----------------------------------------------------------------------------
# Email Address Separator
# ***********************

# Author: Alex Wood

# This application translates a text file of email addresses 
# (one per line) into groups of 50 comma-separated email addresses 
# to help expedite the process of mass emailing tasks.
#----------------------------------------------------------------------------

import sys
from PyQt4 import QtGui, QtCore

def main():
    
    application = QtGui.QApplication(sys.argv)
    emailAddressWindow = MainWindow()
    sys.exit(application.exec_())

class MainWindow(QtGui.QWidget):    
    def __init__(self):
        super(MainWindow, self).__init__()
        
        self.initUI()
        
    def initUI(self):

        grid = QtGui.QGridLayout()
        grid.setSpacing(10)

        self.textArea = QtGui.QTextBrowser()        
        grid.addWidget(self.textArea, 3, 1, 5, 10)

        selectFileButton = QtGui.QPushButton('Choose a File', self)
        selectFileButton.clicked.connect(self.processAndDisplayAddresses)
        grid.addWidget(selectFileButton, 9, 1, 1, 7)

        helpButton = QtGui.QPushButton('About', self)
        helpButton.clicked.connect(self.displayHelpWindow)
        grid.addWidget(helpButton, 9, 8, 2, 3)

        quitButton = QtGui.QPushButton('Exit', self)
        quitButton.clicked.connect(QtCore.QCoreApplication.instance().quit)
        grid.addWidget(quitButton, 10, 1, 1, 7)

        self.setWindowTitle('Email Address Separator')
        self.setGeometry(600, 600, 650, 600)
        self.setLayout(grid)
        self.layout().setSizeConstraint(QtGui.QLayout.SetMinAndMaxSize)
        self.center()

        self.show()

    def processAndDisplayAddresses(self):

        fileName = QtGui.QFileDialog.getOpenFileName(self, 'Open File', 
                                                     QtCore.QDir.homePath())
        
        file = open(fileName, 'r')
        
        with file:        
            output = []

            NUMBER_OF_ADDRESSES_PER_LINE = 50
            address_counter = 1;

            for line in file:
              if line.strip():
                  if (address_counter % NUMBER_OF_ADDRESSES_PER_LINE) == 0:
                      output.append(line)
                      output.append('\n')
                  else:
                      output.append(line.replace('\n', ', '))

                  address_counter += 1

            self.textArea.setText(''.join([element for element in output]))

    def displayHelpWindow(self):

        dialog = HelpWindow(self)
        result = dialog.exec_()
                                 
    def center(self):
        
        windowGeometry = self.frameGeometry()
        windowGeometry.moveCenter(QtGui.QDesktopWidget().availableGeometry()
                                                        .center())
        self.move(windowGeometry.topLeft())

class HelpWindow(QtGui.QDialog):
    def __init__(self, parent = None):
        QtGui.QDialog.__init__(self, parent)

        self.initUI()

    def initUI(self):
        self.setWindowTitle('About')
        self.resize(300, 200)

        box = QtGui.QVBoxLayout()

        browser = QtGui.QTextBrowser()
        browser.setText("This application translates a text file of email "
            + "addresses (one per line) into groups of 50 comma-separated email "
            + "addresses to help expedite the process of mass emailing tasks. "
            + "<br><qt>&copy; Alex Wood, 2012</qt>")
        box.addWidget(browser)

        closeButton = QtGui.QPushButton('OK', self)
        closeButton.clicked.connect(self.close)
        box.addWidget(closeButton)

        box.setSizeConstraint(QtGui.QLayout.SetFixedSize)
        self.setLayout(box)


if __name__ == '__main__':
    main()
