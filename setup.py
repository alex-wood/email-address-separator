from setuptools import setup
 
APP = ['AddressSeparator.py']
OPTIONS = {'argv_emulation': True, 'includes': ['sip', 'PyQt4.QtGui']}
 
setup(
    app=APP,
    options={'py2app': OPTIONS},
    setup_requires=['py2app'],
    name = "Address Separator",
    author_email = "alex.wood.86@gmail.com",
)
